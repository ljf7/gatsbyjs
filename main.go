package main

import (
	"bytes"
	"fmt"
	"github.com/AlecAivazis/survey/v2"
	"io/ioutil"
	"os/exec"
	"strings"
)

var qs = []*survey.Question{
	{
		Name: "domain",
		Prompt: &survey.Select{
			Message: "Choose a domain:",
			Options: []string{"@segfault.net", "@smokes.thc.org", "@guest.team-teso.net"},
			Default: "@segfault.net",
		},
		Validate: survey.Required,
	},
	{
		Name:      "email",
		Prompt:    &survey.Input{Message: "Please input your email:"},
		Validate:  survey.MinLength(8),
		Transform: survey.ToLower,
	},
	{
		Name:      "username",
		Prompt:    &survey.Input{Message: "Please input a username you like:"},
		Validate:  survey.MinLength(2),
		Transform: survey.ToLower,
	},
}

func main() {
	fmt.Print("This is a free mail forwarding service by The Hacker's Choice, which was founded in 1995.\n\n")
	fmt.Print("The Hacker's Choice's README:\n")
	fmt.Print("• We are a group of international hackers.\n• We do IT security work.\n• We are not for hire.\n• All of our work is for the public.\n• We research and publish tools and academic papers to expose fishy IT security that just isn’t secure.\n• We also develop and publish tools to help the IT Security movement.\n Find out more at https://www.thc.org/\n\n")
	fmt.Print("The Hacker's Choice's README ends\n\n")
	fmt.Print("This Repl is developed by @euyix on Replit, no rights reserved.\n\n")
	answers := struct {
		Domain   string `survey:"domain"`
		Email    string `survey:"email"`
		Username string `survey:"username"`
	}{}

	// perform the questions
	err := survey.Ask(qs, &answers)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Printf("You chose %s.\n", answers.Domain)
	fmt.Printf("Your email is %s.\n", answers.Email)
	fmt.Printf("Your username will be %s.\n\n", answers.Username)

	cmd := exec.Command("curl", "-k", fmt.Sprintf("https://mail.thc.org/register?name=%s&to=%s", answers.Username, answers.Email))
	var out bytes.Buffer
	cmd.Stdout = &out

	err = cmd.Run()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	cmdout := fmt.Sprintf("%s", out.String())
	cmdoutPath := "cmdout.html"
	err = writeFile(cmdoutPath, cmdout)
	if err != nil {
		fmt.Printf("Error writing to %s: %v", cmdoutPath, err)
		return
	}

	bat := exec.Command("sh", "bat.sh")
	var output bytes.Buffer
	bat.Stdout = &output

	err = bat.Run()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Print(output.String(), "\n\n")
	if !(strings.Contains(output.String(), "ERROR")) {
		fmt.Printf("Your email %s%s is ready to use.\n", answers.Username, answers.Domain)
		fmt.Println("Now you can use this to register a new Replit account, try it out!")
	}
}

func writeFile(filename, data string) error {
	return ioutil.WriteFile(filename, []byte(data), 0644)
}
